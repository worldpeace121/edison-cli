#! /usr/bin/env node

const fs = require("fs")
const chalk = require("chalk")
const {version} = require("./package.json")
const commander = require("commander")
const inquirer = require("inquirer")
const { exec } = require('child_process');
const ora = require("ora")
// const download = require("download-git-repo")
const path = require("path")

var checkDire = function (dir,name) {
    let isExists = fs.existsSync(dir);
    if (isExists) {
      console.log(chalk.red(
        `The ${name} project already exists in  directory. Please try to use another projectName`
      ));
      process.exit(1);
    }
}

var promptList = [{
        type: 'input',
        message: '请输入项目名称:',
        name: 'name',
        validate:(v) => {
            if(v == ""){
                return "请输入名称"
            }
            return true
        }
    }]
    
commander.version(version, '-v, --version')
    .command('init')
    .alias("i")
    .action(async () => {
        inquirer.prompt(promptList).then(async (type) => {
            let projectName = type.name
            await checkDire(path.join(process.cwd(),projectName),projectName); 
            console.log(chalk.white('\n Start generating... \n'))
            const spinner = ora("Downloading...");
            spinner.start();
            let url = "https://gitlab.com/worldpeace121/template.git"
            let gitName = "template"
            exec('git clone ' + url, function (error, stdout, stderr) {
                if(error !== null){
                    console.log(chalk.red(
                        `clone fail,${error}`
                      ));
                    return;
                    spinner.fail()
                }
                fs.rename(gitName, projectName, (err)=>{
                    if (err) {
                        exec('rm -rf '+gitName, function (err, out) {});
                        console.log(chalk.red(`The ${projectName} project template already exist`));
                        spinner.fail()
                    } else {
                        spinner.succeed()
                        console.log(chalk.green(`The ${projectName} project template successfully create(项目模版创建成功)`));
                        console.log(chalk.cyan('\n Generation completed!'))
                        console.log(chalk.cyan('\n To get started'))
                        console.log(chalk.cyan(`\n cd ${projectName} && npm i \n `))
                    }
                });
                
            })
        })
    })

commander.parse(process.argv);